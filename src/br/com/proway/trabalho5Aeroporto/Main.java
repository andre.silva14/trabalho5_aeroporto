package br.com.proway.trabalho5Aeroporto;

import br.com.proway.trabalho5Aeroporto.models.Aeroporto;
import br.com.proway.trabalho5Aeroporto.models.Gol;
import br.com.proway.trabalho5Aeroporto.models.LaTam;
import br.com.proway.trabalho5Aeroporto.models.LinhaAerea;
import br.com.proway.trabalho5Aeroporto.utils.Horarios;

import javax.swing.*;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Aeroporto guarulhos = new Aeroporto("Guarulhos", "São Paulo");

        LaTam laTam = new LaTam(100, "Boeing 777-300ER");
        Gol gol = new Gol(105, "Boeing 737-800 ");

        guarulhos.inserirLinha(new LinhaAerea("guarulhos", "Amsterdã", 8750, "Amsterdã(Netherlands)"));
        guarulhos.inserirLinha(new LinhaAerea("guarulhos", "Nova York", 4945, "Nova York(EUA)"));
        guarulhos.inserirLinha(new LinhaAerea("guarulhos", "Munich", 6185, "Munich(Germany)"));
        guarulhos.inserirLinha(new LinhaAerea("guarulhos", "Paris", 7250, "Paris(France)"));


        int passaporte;
        while (true) {

            try {
                try {
                    passaporte = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe seu passaporte:"));
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Informe somente números");
                    continue;
                }

                String[] opcoes = {"Gol", "Latam"};
                int opcao = JOptionPane.showOptionDialog(null, "Selecione sua companhia:", "Companhia", JOptionPane.DEFAULT_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);
                if (opcao == 0) {
                    String[] horarios = new String[Horarios.values().length];
                    for (int i = 0; i < Horarios.values().length; i++) {
                        horarios[i] = Horarios.values()[i].getHorario();
                    }
                    boolean disponivel = true;
                    while (disponivel == true){
                        String hora = (String) JOptionPane.showInputDialog(null, "Selecione seu horário",
                                "Horários", JOptionPane.QUESTION_MESSAGE, null, horarios, horarios[0]);
                        Horarios horaEnum = Arrays.stream(Horarios.values()).filter(h -> h.getHorario() == hora).findFirst().get();
                        boolean tem = gol.HorarioIndisponivel(horaEnum);
                        if (tem) {
                            break;
                        }
                    }

                } else if (opcao == 1) {
                    String[] horarios = new String[Horarios.values().length];
                    for (int i = 0; i < Horarios.values().length; i++) {
                        horarios[i] = Horarios.values()[i].getHorario();
                    }
                    boolean disponivel = true;
                    while (disponivel == true) {
                        String hora = (String) JOptionPane.showInputDialog(null, "Selecione seu horário",
                                "Horários", JOptionPane.QUESTION_MESSAGE, null, horarios, horarios[0]);

                        Horarios horaEnum = Arrays.stream(Horarios.values()).filter(l -> l.getHorario() == hora).findFirst().get();

                        boolean tem = laTam.HorarioIndisponivel(horaEnum);
                        if (tem) {
                            break;
                        }
                    }


                } else if (opcao == 2) {
                    break;
                }

                String[] optionsToChoose = new String[guarulhos.getLinhas().size()];

                for (int i = 0; i < guarulhos.getLinhas().size(); i++) {
                    optionsToChoose[i] = guarulhos.getLinhas().get(i).getDescricao();
                }

                String local = (String) JOptionPane.showInputDialog(
                        null,
                        "Indique o seu Destino", "Where do you want to go?", JOptionPane.QUESTION_MESSAGE,
                        null, optionsToChoose, optionsToChoose[0]);


                LinhaAerea linhaAerea = guarulhos.getLinhas().stream().filter(l -> l.getDescricao() == local).findFirst().get();
                JOptionPane.showMessageDialog(null, "Passaporte: " + passaporte + "\nDestino: " + linhaAerea.getDestino() + " \nPreço: " + linhaAerea.getPreco());

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ocorreu algum erro");
            }


        }


    }
}
