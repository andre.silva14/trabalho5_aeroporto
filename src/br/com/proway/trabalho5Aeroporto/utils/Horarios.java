package br.com.proway.trabalho5Aeroporto.utils;

public enum Horarios {
    HORARIO1(1, "09:05"),
    HORARIO2(2, "10:15"),
    HORARIO3(3, "11:55"),
    HORARIO4(4, "12:50"),
    HORARIO5(5, "13:20"),
    HORARIO6(6, "14:35"),
    HORARIO7(7, "15:55"),
    HORARIO8(8, "17:00"),
    HORARIO9(9, "18:45"),
    HORARIO10(10, "19:35"),
    HORARIO11(11, "20:20"),
    HORARIO12(12, "22:05");

    private final int valor;
    private final String horario;


    Horarios(int valor, String horario) {
        this.valor = valor;
        this.horario = horario;
    }



    public int getValor() {
        return valor;
    }

    public String getHorario() {
        return horario;
    }
}
