package br.com.proway.trabalho5Aeroporto.interfaces;

import br.com.proway.trabalho5Aeroporto.utils.Horarios;

public interface HorariosDisponiveis {

    boolean HorarioIndisponivel(Horarios horario);

}
