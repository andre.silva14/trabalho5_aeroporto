package br.com.proway.trabalho5Aeroporto.models;

import br.com.proway.trabalho5Aeroporto.interfaces.HorariosDisponiveis;
import br.com.proway.trabalho5Aeroporto.utils.Horarios;

import javax.swing.*;

public class Gol extends CompanhiaAerea implements HorariosDisponiveis {


    public Gol( int id, String modeloAviao) {
        super(id, modeloAviao);
    }

    @Override
    public boolean HorarioIndisponivel(Horarios horario) {
        if (horario.getValor() == 9 || horario.getValor() == 3 ) {
            JOptionPane.showMessageDialog(null, "Infelizmente " +
                    "o horário está indisponivel devido ao mau tempo \n " + "Agradecemos por sua compreensão!");
            return false;
        }else if( horario.getValor() == 1 || horario.getValor() == 5 || horario.getValor()==7 || horario.getValor()==11){
            JOptionPane.showMessageDialog(null,"Este horário encontra-se indisponível para esta companhia aérea.");
            return false;
        } else {
            return true;
        }
    }
}
