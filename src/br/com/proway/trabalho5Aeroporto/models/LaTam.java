package br.com.proway.trabalho5Aeroporto.models;

import br.com.proway.trabalho5Aeroporto.interfaces.HorariosDisponiveis;
import br.com.proway.trabalho5Aeroporto.utils.Horarios;

import javax.swing.*;
import java.util.ArrayList;

public class LaTam extends CompanhiaAerea implements HorariosDisponiveis {


    public LaTam(int id, String modeloAviao) {
        super(id, modeloAviao);
    }


    @Override
    public boolean HorarioIndisponivel(Horarios horario) {
        if (horario.getValor() == 2 || horario.getValor() == 10) {
            JOptionPane.showMessageDialog(null, "Infelizmente " +
                    "o horário de voo foi cancelado devido ao mau tempo, por favor tente outro horário!\n " + "Agradecemos por sua compreensão!");
            return false;
        }else if(horario.getValor()==4 || horario.getValor()==6 || horario.getValor()== 8 || horario.getValor()==12){
            JOptionPane.showMessageDialog(null, "Este horário encontra-se indisponível para esta companhia aérea.");
            return false;
        } else {
            return true;
        }
    }
}
