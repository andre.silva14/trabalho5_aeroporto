package br.com.proway.trabalho5Aeroporto.models;

public class LinhaAerea {
    private String origem;
    private String destino;
    private double preco;
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LinhaAerea(String origem, String destino, double preco, String descricao) {
       this.origem = origem;
        this.destino = destino;
        this.preco = preco;
        this.descricao = descricao;
    }

    public String getDestino() {
        return destino;
    }

    public double getPreco() {
        return preco;
    }


}
