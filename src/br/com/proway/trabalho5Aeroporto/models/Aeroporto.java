package br.com.proway.trabalho5Aeroporto.models;

import java.util.ArrayList;

public class Aeroporto {
    private String nome;
    private String cidade;
    private ArrayList<LinhaAerea> linhas;

    public Aeroporto(String nome, String cidade) {
        this.nome = nome;
        this.cidade = cidade;
        linhas = new ArrayList<LinhaAerea>();
    }

    public ArrayList<LinhaAerea> getLinhas() {
        return linhas;
    }
    public void inserirLinha(LinhaAerea linha) {
       this.linhas.add(linha);
    }

}
